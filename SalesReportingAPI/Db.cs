﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;

namespace SalesReportingAPI
{
    public class Db
    {
        public Db(String connectionString)
        {
            ConnectionString = connectionString;
        }

        public String ConnectionString { get; set; }

        private IDbConnection _connection;

        public void OpenConnection()
        {
            _connection = new SqlConnection(ConnectionString);
            _connection.Open();
        }

        public void CloseConnection()
        {
            if (_connection.State == ConnectionState.Open)
            {
                _connection.Close();
            }
        }

        public IEnumerable<dynamic> RunQuery(string Query)
        {
            return _connection.Query(Query); // Runs a select query using Dapper.net, returned result will be dynamic
        }

        public IDbConnection CurrentConnection()
        {
            return _connection;
        }
    }
}