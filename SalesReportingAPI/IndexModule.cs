﻿using System;
using System.Collections;
using System.Web;

namespace SalesReportingAPI
{
    using Nancy;

    public class IndexModule : NancyModule
    {
        public IndexModule()
        {
            
            Get["/query/{query_id}"] = parameters =>
            {
                // Initialize the database
                //FIXME: User and password are hard coded
                Db db = new Db("Server=tcp:cjm75e2fng.database.windows.net,1433;Database=TestReporting;User ID=monadmin@cjm75e2fng;Password=_MansourWP31;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
                // Open connection
                db.OpenConnection();
                // Specifiy the query to be run
                int queryId = Int32.Parse(parameters.query_id); // Example: http://example.com/query/5

                string Query = null;
                IEnumerable Result = null;

                switch (queryId)
                {
                    case 1:
                    {
                        Query = "SELECT SUM(Quantity), Year " +
                                "FROM  SALES, TIMES " +
                                "WHERE TIMES.IDTimes = SALES.IDTimes " +
                                "AND Year BETWEEN 2010 AND 2012 " +
                                "GROUP BY Year;";
                        Result = db.RunQuery(Query);
                        break;
                    }
                    case 2:
                    {
                        Query = "SELECT SUM(Amount), SUM(Quantity), Year " +
                                "FROM SALES, TIMES " +
                                "WHERE TIMES.IDTimes = SALES.IDTimes " +
                                "AND Year = 2008 " +
                                "GROUP BY Year;";
                        Result = db.RunQuery(Query);
                        break;
                    }
                    case 3:
                    {
                        Query = "SELECT SUM(Amount), Year " +
                                "FROM SALES, TIMES " +
                                "WHERE TIMES.IDTimes = SALES.IDTimes " +
                                "AND Year < 2000 " +
                                "GROUP BY Year;";
                        Result = db.RunQuery(Query);
                        break;
                    }
                    case 4:
                    {
                        Query = "SELECT SUM(Amount), Month " +
                                "FROM SALES, TIMES " +
                                "WHERE TIMES.IDTimes = SALES.IDTimes " +
                                "AND Year >= 2000 " +
                                "GROUP BY Month;";
                        Result = db.RunQuery(Query);
                        break;
                    }
                    case 5:
                    {
                        Query = "SELECT SUM(Amount), Year " +
                                "FROM  SALES, TIMES " +
                                "WHERE TIMES.IDTimes = SALES.IDTimes " +
                                "GROUP BY Year;";
                        Result = db.RunQuery(Query);
                        break;
                    }
                    case 6:
                    {
                        Query = "SELECT SUM(Amount), Town " +
                                "FROM  SALES, CUSTOMERS, TIMES " +
                                "WHERE CUSTOMERS.IDCustomers = SALES.IDCustomers " +
                                "AND TIMES.IDTimes = SALES.IDTimes " +
                                "AND Year BETWEEN 2010 AND 2012 " +
                                "GROUP BY Town;";
                        Result = db.RunQuery(Query);
                        break;
                    }
                    case 7:
                    {
                        Query = "SELECT SUM(Quantity), Month, Department " +
                                "FROM  SALES, CUSTOMERS, TIMES " +
                                "WHERE CUSTOMERS.IDCustomers = SALES.IDCustomers " +
                                "AND TIMES.IDTimes = SALES.IDTimes " +
                                "AND Year BETWEEN 2000 AND 2005 " +
                                "GROUP BY Month, Department;";
                        Result = db.RunQuery(Query);
                        break;
                    }
                    case 8:
                    {
                        Query=  "SELECT SUM(Amount), Year, Town " +
                                "FROM SALES, CUSTOMERS, TIMES " +
                                "WHERE CUSTOMERS.IDCustomers = SALES.IDCustomers " +
                                "AND TIMES.IDTimes = SALES.IDTimes " +
                                "AND Year BETWEEN 2002 AND 2012 " +
                                "GROUP BY Year, Town;";
                        Result = db.RunQuery(Query);
                        break;
                    }
                    case 9:
                    {
                        Query = "SELECT SUM(Quantity), Year, Sector " +
                                "FROM  SALES, PRODUCTS, TIMES " +
                                "WHERE TIMES.IDTimes = SALES.IDTimes " +
                                "AND PRODUCTS.IDProducts = SALES.IDProducts " +
                                "AND Year < 2000 " +
                                "GROUP BY Year, Sector;";
                        Result = db.RunQuery(Query); 
                        break;
                    }
                    case 10:
                    {
                        Query = "SELECT SUM(Quantity), Year, Range " +
                                "FROM SALES, PRODUCTS, TIMES " +
                                "WHERE PRODUCTS.IDProducts = SALES.IDProducts " +
                                "AND TIMES.IDTimes = SALES.IDTimes " +
                                "GROUP BY Year, Range;";
                        Result = db.RunQuery(Query);
                        break;
                    }
                    case 11:
                    {
                        Query = "SELECT SUM(Amount), Year, Range " +
                                "FROM SALES, PRODUCTS, TIMES " +
                                "WHERE PRODUCTS.IDProducts = SALES.IDProducts " +
                                "AND TIMES.IDTimes = SALES.IDTimes " +
                                "AND Year BETWEEN 1990 AND 2009 " +
                                "GROUP BY Year, Range;";
                        Result = db.RunQuery(Query);
                        break;
                    }
                    case 12:
                    {
                        Query = "SELECT SUM(Amount), Town, Sector, Month " +
                                "FROM  SALES, CUSTOMERS, PRODUCTS, TIMES " +
                                "WHERE CUSTOMERS.IDCustomers = SALES.IDCustomers " +
                                "AND PRODUCTS.IDProducts = SALES.IDProducts " +
                                "AND TIMES.IDTimes = SALES.IDTimes " +
                                "AND Year = 2012" +
                                "GROUP BY Town, Sector, Month;";
                        Result = db.RunQuery(Query);
                        break;
                    }
                    case 13:
                    {
                        Query = "SELECT SUM(Amount), Year, Sector, Department " +
                                "FROM  SALES, CUSTOMERS, PRODUCTS, TIMES " +
                                "WHERE CUSTOMERS.IDCustomers = SALES.IDCustomers " +
                                "AND PRODUCTS.IDProducts = SALES.IDProducts " +
                                "AND TIMES.IDTimes = SALES.IDTimes " +
                                "AND Year BETWEEN 2000 AND 2005 " +
                                "GROUP BY Year, Sector, Department;";
                        Result = db.RunQuery(Query);
                        break;
                    }
                    case 14:
                    {
                        Query = "SELECT SUM(Amount), Town, Sector " +
                                "FROM SALES, CUSTOMERS, PRODUCTS, TIMES " +
                                "WHERE CUSTOMERS.IDCustomers = SALES.IDCustomers " +
                                "AND PRODUCTS.IDProducts = SALES.IDProducts " +
                                "AND TIMES.IDTimes = SALES.IDTimes " +
                                "AND TIMES.Year BETWEEN 2002 AND 2012 " +
                                "GROUP BY Town, Sector;";
                        Result = db.RunQuery(Query);
                        break;
                    }
                    case 15:
                    {
                        Query = "SELECT SUM(Amount), Department, Month " +
                                "FROM SALES, CUSTOMERS, PRODUCTS, TIMES " +
                                "WHERE CUSTOMERS.IDCustomers = SALES.IDCustomers " +
                                "AND PRODUCTS.IDProducts = SALES.IDProducts " +
                                "AND TIMES.IDTimes = SALES.IDTimes " +
                                "AND Range = 'range1' " +
                                "AND Year >= 2000 " +
                                "GROUP BY Department, Month;";
                        Result = db.RunQuery(Query);
                        break;
                    }
                }
                return Response.AsJson(Result);
            };
        }
    }
}